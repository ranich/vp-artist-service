package com.viaplay.artistservice.controller;

import com.viaplay.artistservice.ArtistServiceApplication;
import com.viaplay.artistservice.exceptions.RestApiException;
import com.viaplay.artistservice.model.ArtistResponse;
import com.viaplay.artistservice.service.ArtistService;
import com.viaplay.artistservice.util.Json;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.viaplay.artistservice.util.TestData.MBID;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ArtistServiceApplication.class)
@SpringBootTest
public class ArtistControllerTest {

    @MockBean
    private ArtistService artistService;
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void shouldReturnNormalResponse() throws Exception {
        final ArtistResponse artistResponse = ArtistResponse.builder().mbid(MBID).build();
        when(artistService.artistResponse(artistResponse.getMbid()))
                .thenReturn(artistResponse);
        mockMvc.perform(get(formatGetArtistUrlForId(MBID)))
                .andExpect(status().isOk())
                .andExpect(content().string(Json.serialize(artistResponse)));

        verify(artistService, times(1)).artistResponse(artistResponse.getMbid());
        verifyNoMoreInteractions(artistService);
    }

    @Test
    public void shouldNotGetExecutedWhenMbidIsNotUUIDFormat() throws Exception {
        final ArtistResponse artistResponse = ArtistResponse.builder()
                .mbid(MBID).build();
        when(artistService.artistResponse(artistResponse.getMbid()))
                .thenReturn(artistResponse);
        mockMvc.perform(get(formatGetArtistUrlForId("123123")));

        verify(artistService, times(0)).artistResponse(any());
    }

    @Test
    public void shouldReturnNotFound() throws Exception {
        when(artistService.artistResponse(MBID)).thenThrow(new RestApiException(HttpStatus.NOT_FOUND.value()));
        mockMvc.perform(get(formatGetArtistUrlForId(MBID)))
                .andExpect(status().isNotFound());

        verify(artistService, times(1)).artistResponse(any());
    }

    @Test
    public void shouldReturnBadRequest() throws Exception {
        when(artistService.artistResponse(MBID))
                .thenThrow(new RestApiException(HttpStatus.BAD_REQUEST.value()));
        mockMvc.perform(get(formatGetArtistUrlForId(MBID)))
                .andExpect(status().isBadRequest());

        verify(artistService, times(1)).artistResponse(any());
    }

    @Test
    public void shouldReturnServiceUnavailable() throws Exception {
        when(artistService.artistResponse(MBID))
                .thenThrow(new RestApiException(HttpStatus.SERVICE_UNAVAILABLE.value()));
        mockMvc.perform(get(formatGetArtistUrlForId(MBID)))
                .andExpect(status().isServiceUnavailable());

        verify(artistService, times(1)).artistResponse(any());
    }

    private static String formatGetArtistUrlForId(String id) {
        return String.format("/api/v1/artists/%s", id);
    }
}