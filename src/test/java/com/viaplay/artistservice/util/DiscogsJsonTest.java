package com.viaplay.artistservice.util;

import com.viaplay.artistservice.model.discogs.DiscogsResponse;
import org.junit.Test;

import static com.viaplay.artistservice.util.JsonTestUtil.assertIgnoresUnknownProperties;
import static com.viaplay.artistservice.util.JsonTestUtil.roundtrip;
import static com.viaplay.artistservice.util.TestData.DISCOGS_RESPONSE;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class DiscogsJsonTest {
    @Test
    public void testRoundTripCoverArtResponse() throws Exception {
        DiscogsResponse before = DISCOGS_RESPONSE;
        DiscogsResponse after = roundtrip(before, DiscogsResponse.class);
        assertThat(after, is(before));
    }

    @Test
    public void shouldNotFailOnMissingPropertis() throws Exception {
        DiscogsResponse before = DiscogsResponse.builder().id("123123").build();
        DiscogsResponse after = roundtrip(before, DiscogsResponse.class);
        assertThat(after, is(before));
    }

    @Test
    public void deserializeShouldIgnoreUnknownPropertiesRelation() throws Throwable {
        assertIgnoresUnknownProperties(
                DISCOGS_RESPONSE,
                node -> Json.deserialize(Json.serialize(node),
                        DiscogsResponse.class));
    }
}
