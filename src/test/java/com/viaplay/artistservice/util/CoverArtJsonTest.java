package com.viaplay.artistservice.util;

import com.viaplay.artistservice.model.coverart.CoverArtResponse;
import org.junit.Test;

import java.util.Collections;

import static com.viaplay.artistservice.util.JsonTestUtil.assertIgnoresUnknownProperties;
import static com.viaplay.artistservice.util.JsonTestUtil.roundtrip;
import static com.viaplay.artistservice.util.TestData.COVER_ART_IMAGE;
import static com.viaplay.artistservice.util.TestData.COVER_ART_RESPONSE;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class CoverArtJsonTest {
    @Test
    public void testRoundTripCoverArtResponse() throws Exception {
        CoverArtResponse before = COVER_ART_RESPONSE;
        CoverArtResponse after = roundtrip(before, CoverArtResponse.class);
        assertThat(after, is(before));
    }

    @Test
    public void shouldNotFailOnMissingPropertis() throws Exception {
        CoverArtResponse before = CoverArtResponse.builder()
                .images(Collections.singletonList(COVER_ART_IMAGE))
                .build();
        CoverArtResponse after = roundtrip(before, CoverArtResponse.class);
        assertThat(after, is(before));
    }

    @Test
    public void deserializeShouldIgnoreUnknownPropertiesRelation() throws Throwable {
        assertIgnoresUnknownProperties(
                COVER_ART_RESPONSE,
                node -> Json.deserialize(Json.serialize(node),
                        CoverArtResponse.class));
    }
}
