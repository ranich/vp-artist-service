package com.viaplay.artistservice.util;

import com.fasterxml.jackson.databind.node.ObjectNode;
import javaslang.control.Try;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@Slf4j
public class JsonTestUtil {
    static <T> void assertIgnoresUnknownProperties(T value, Try.CheckedFunction<ObjectNode, T> deserialize)
            throws Throwable {
        final ObjectNode node = Json.OBJECT_MAPPER.convertValue(value, ObjectNode.class);
        node.put("non_existent_field", "foobar");
        final T deserialized = deserialize.apply(node);
        assertThat(deserialized, is(value));
    }

    static <T> T roundtrip(T t, Class<? extends T> clazz) throws IOException {
        String json = Json.serialize(t);
        log.info(json);
        return Json.deserialize(json, clazz);
    }

    static <T> String roundtrip(String json, Class<? extends T> clazz) throws IOException {
        log.debug(json);
        T t = Json.deserialize(json, clazz);
        final String s = Json.serialize(t);
        log.info(s);
        return s;
    }
}
