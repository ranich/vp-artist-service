package com.viaplay.artistservice.util;

import com.viaplay.artistservice.model.coverart.CoverArtResponse;
import com.viaplay.artistservice.model.coverart.Image;
import com.viaplay.artistservice.model.coverart.Thumbnails;
import com.viaplay.artistservice.model.discogs.DiscogsResponse;
import com.viaplay.artistservice.model.musicbrainz.*;

import java.util.Collections;
import java.util.UUID;

public class TestData {
    public static final String MBID = UUID.randomUUID().toString();
    public static final LifeSpan LIFE_SPAN = LifeSpan.builder()
            .begin("1946-09-05")
            .end("1991-11-24")
            .ended(true)
            .build();
    public static final Area AREA = Area.builder()
            .id("8a754a16-0027-3a29-b6d7-2b40ea0481ed")
            .name("United Kingdom")
            .sortName("United Kingdom")
            .disambiguation("")
            .iso31661Codes(Collections.singletonList("GB"))
            .build();
    public static final BeginArea BEGIN_AREA = BeginArea.builder()
            .id("71b1aedf-a1ad-4eb6-83a0-57134c8b34e2")
            .name("Stone Town")
            .sortName("Stone Town")
            .disambiguation("")
            .iso31662Codes(Collections.singletonList("NO-03"))
            .build();
    public static final Area END_AREA = Area.builder()
            .id("a37eca4d-c1e2-48a3-bcba-61796db97ebf")
            .name("Kensington")
            .sortName("Kensington")
            .disambiguation("")
            .build();
    public static final MusicBrainzResponse MUSIC_BRAINZ = MusicBrainzResponse.builder()
            .id("022589ac-7177-460d-a178-9976cf70e29f")
            .name("Freddie Mercury")
            .sortName("Mercury, Freddie")
            .typeId("b6e035f4-3ce9-331c-97df-83397230b0df")
            .type("Person")
            .genderId("36d3d30a-839d-3eda-8cb3-29be4384e4a9")
            .gender("Male")
            .lifeSpan(LIFE_SPAN)
            .country("GB")
            .area(AREA)
            .beginArea(BEGIN_AREA)
            .endArea(END_AREA)
            .disambiguation("")
            .ipis(Collections.singletonList("00077406269"))
            .isnis(Collections.singletonList("0000000109859100"))
            .build();
    public static final Url URL = Url.builder()
            .id("8af26be7-92ff-4cbd-94be-70aba93c2b8c")
            .resource("https://www.bbc.co.uk/music/artists/022589ac-7177-460d-a178-9976cf70e29f")
            .build();
    public static final Relation RELATION = Relation.builder()
            .typeId("d028a975-000c-4525-9333-d3c8425e4b54")
            .type("BBC Music page")
            .url(URL)
            .sourceCredit("")
            .targetType("url")
            .targetCredit("")
            .direction("forward")
            .ended(false)
            .build();
    public static final ReleaseGroup RELEASE_GROUP = ReleaseGroup.builder()
            .id("0c6da806-bdfe-3e64-88ed-74e778b6bfac")
            .title("Romantic Ballads")
            .primaryTypeId("f529b476-6e62-324f-b0aa-1f3e33d313fc")
            .primaryType("Album")
            .firstReleaseDate("1998")
            .secondaryTypeIds(Collections.singletonList("dd2a21e1-0c00-3729-a7a0-de60b84eb5d1"))
            .secondaryTypes(Collections.singletonList("Compilation"))
            .disambiguation("")
            .build();
    public static final MusicBrainzResponse MUSIC_BRAINZ_FULL = MUSIC_BRAINZ.toBuilder()
            .relations(Collections.singletonList(RELATION))
            .releaseGroups(Collections.singletonList(RELEASE_GROUP))
            .build();

    // COVER ART
    public static final Thumbnails COVER_ART_THUMBNAILS = Thumbnails.builder()
            .t250("http://coverartarchive.org/release/f268b8bc-2768-426b-901b-c7966e76de29/12750224075-250.jpg")
            .t500("http://coverartarchive.org/release/f268b8bc-2768-426b-901b-c7966e76de29/12750224075-500.jpg")
            .t1200("http://coverartarchive.org/release/f268b8bc-2768-426b-901b-c7966e76de29/12750224075-1200.jpg")
            .small("http://coverartarchive.org/release/f268b8bc-2768-426b-901b-c7966e76de29/12750224075-250.jpg")
            .large("http://coverartarchive.org/release/f268b8bc-2768-426b-901b-c7966e76de29/12750224075-500.jpg")
            .build();
    public static final Image COVER_ART_IMAGE = Image.builder()
            .id("11750979486")
            .image("http://coverartarchive.org/release/0ae1a95b-84ab-4768-aa1a-fe86046a96f9/11751004708.png")
            .thumbnails(COVER_ART_THUMBNAILS)
            .edit(35509108)
            .comment("")
            .types(Collections.singletonList("Front"))
            .front(true)
            .approved(true)
            .back(false)
            .build();
    public static final CoverArtResponse COVER_ART_RESPONSE = CoverArtResponse.builder()
            .release("http://musicbrainz.org/release/0ae1a95b-84ab-4768-aa1a-fe86046a96f9")
            .images(Collections.singletonList(COVER_ART_IMAGE))
            .build();
    public static final String COVER_ART_RESPONSE_JSON = "{\"images\":[{\"approved\":true,\"back\":false,\"comment\":\"\",\"edit\":56537677,\"front\":true,\"id\":21133319792,\"image\":\"http://coverartarchive.org/release/c17f0458-01f4-41f8-8112-bd2f2c5d93f9/21133319792.jpg\",\"thumbnails\":{\"1200\":\"http://coverartarchive.org/release/c17f0458-01f4-41f8-8112-bd2f2c5d93f9/21133319792-1200.jpg\",\"250\":\"http://coverartarchive.org/release/c17f0458-01f4-41f8-8112-bd2f2c5d93f9/21133319792-250.jpg\",\"500\":\"http://coverartarchive.org/release/c17f0458-01f4-41f8-8112-bd2f2c5d93f9/21133319792-500.jpg\",\"large\":\"http://coverartarchive.org/release/c17f0458-01f4-41f8-8112-bd2f2c5d93f9/21133319792-500.jpg\",\"small\":\"http://coverartarchive.org/release/c17f0458-01f4-41f8-8112-bd2f2c5d93f9/21133319792-250.jpg\"},\"types\":[\"Front\"]}],\"release\":\"https://musicbrainz.org/release/c17f0458-01f4-41f8-8112-bd2f2c5d93f9\"}";

    // DISCOGS RESPONSE
    public static final DiscogsResponse DISCOGS_RESPONSE = DiscogsResponse.builder()
            .id("79949")
            .profile("Best known as lead singer of popular British rock group [a=Queen]. Born 5 September 1946..")
            .build();

}
