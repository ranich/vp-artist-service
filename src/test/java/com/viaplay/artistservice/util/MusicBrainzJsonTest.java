package com.viaplay.artistservice.util;

import com.viaplay.artistservice.model.musicbrainz.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import static com.viaplay.artistservice.util.JsonTestUtil.assertIgnoresUnknownProperties;
import static com.viaplay.artistservice.util.JsonTestUtil.roundtrip;
import static com.viaplay.artistservice.util.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@Slf4j
public class MusicBrainzJsonTest {
    // LIFE_SPAN
    @Test
    public void testRoundTripLifeSpanOtO() throws Exception {
        LifeSpan before = LIFE_SPAN;
        LifeSpan after = roundtrip(before, LifeSpan.class);
        assertThat(after, is(before));
    }
    @Test
    public void shouldConvertKebabToCamel() throws Exception {
        String kebab = "{\"begin\":\"1972-10-17\",\"end\":\"1972-10-18\",\"ended\":true}";
        String camel = "{\"begin\":\"1972-10-17\",\"end\":\"1972-10-18\",\"ended\":true}";
        String result = roundtrip(kebab, LifeSpan.class);
        assertThat(result, is(camel));
    }
    @Test
    public void deserializeShouldIgnoreUnknownPropertiesLifeSpan() throws Throwable {
        assertIgnoresUnknownProperties(LIFE_SPAN, node -> Json.deserialize(Json.serialize(node), LifeSpan.class));
    }

    // AREA
    @Test
    public void testRoundTripAreaOtO() throws Exception {
        Area before = AREA;
        Area after = roundtrip(before, Area.class);
        assertThat(after, is(before));
    }
    @Test
    public void shouldConvertKebabToCamelArea() throws Exception {
        final String kebab = "{\"name\":\"Norway\",\"disambiguation\":\"\",\"sort-name\":\"Norway\",\"id\":\"6743d351-6f37-3049-9724-5041161fff4d\",\"iso-3166-1-codes\":[\"NO\"]}";
        final String camel = "{\"id\":\"6743d351-6f37-3049-9724-5041161fff4d\",\"name\":\"Norway\",\"disambiguation\":\"\",\"sortName\":\"Norway\",\"iso31661Codes\":[\"NO\"]}";
        String result = roundtrip(kebab, Area.class);
        assertThat(result, is(camel));
    }
    @Test
    public void deserializeShouldIgnoreUnknownPropertiesArea() throws Throwable {
        assertIgnoresUnknownProperties(AREA, node -> Json.deserialize(Json.serialize(node), Area.class));
    }

    // BEGIN_AREA
    // AREA
    @Test
    public void testRoundTripBeginAreaOtO() throws Exception {
        BeginArea before = BEGIN_AREA;
        BeginArea after = roundtrip(before, BeginArea.class);
        assertThat(after, is(before));
    }
    @Test
    public void shouldConvertKebabToCamelBeginArea() throws Exception {
        final String kebab = "{\"id\":\"f80d529e-f242-46ef-a090-d193ed23075f\",\"sort-name\":\"Oslo\",\"disambiguation\":\"\",\"iso-3166-2-codes\":[\"NO-03\"],\"name\":\"Oslo\"}";
        final String camel = "{\"id\":\"f80d529e-f242-46ef-a090-d193ed23075f\",\"name\":\"Oslo\",\"disambiguation\":\"\",\"sortName\":\"Oslo\",\"iso31662Codes\":[\"NO-03\"]}";
        String result = roundtrip(kebab, BeginArea.class);
        assertThat(result, is(camel));
    }
    @Test
    public void deserializeShouldIgnoreUnknownPropertiesBeginArea() throws Throwable {
        assertIgnoresUnknownProperties(BEGIN_AREA, node -> Json.deserialize(Json.serialize(node), BeginArea.class));
    }

    // END AREA
    @Test
    public void testRoundTripEndAreaOtO() throws Exception {
        Area before = END_AREA;
        Area after = roundtrip(before, Area.class);
        assertThat(after, is(before));
    }
    @Test
    public void shouldConverKebabToCamelEndArea() throws Exception {
        final String kebab = "{\"sort-name\":\"Kensington\",\"id\":\"a37eca4d-c1e2-48a3-bcba-61796db97ebf\",\"name\":\"Kensington\",\"disambiguation\":\"\"}";
        final String camel = "{\"id\":\"a37eca4d-c1e2-48a3-bcba-61796db97ebf\",\"name\":\"Kensington\",\"disambiguation\":\"\",\"sortName\":\"Kensington\"}";
        String result = roundtrip(kebab, Area.class);
        assertThat(result, is(camel));
    }
    @Test
    public void deserializeShouldIgnoreUnknownPropertiesEndArea() throws Throwable {
        assertIgnoresUnknownProperties(END_AREA, node -> Json.deserialize(Json.serialize(node), Area.class));
    }


    // MUSIC_BRAINZ
    @Test
    public void testRoundTripMusicBrainzOtO() throws Exception {
        MusicBrainzResponse before = MUSIC_BRAINZ;
        MusicBrainzResponse after = roundtrip(before, MusicBrainzResponse.class);
        assertThat(after, is(before));
    }
    @Test
    public void shouldConverKebabToCamelMusicBrainz() throws Exception {
        final String kebabAndSpaghetti = "{\"ipis\":[\"00077406269\"],\"type-id\":\"b6e035f4-3ce9-331c-97df-83397230b0df\",\"type\":\"Person\",\"sort-name\":\"Mercury, Freddie\",\"life-span\":{\"ended\":true,\"end\":\"1991-11-24\",\"begin\":\"1946-09-05\"},\"id\":\"022589ac-7177-460d-a178-9976cf70e29f\",\"name\":\"Freddie Mercury\",\"end_area\":{\"sort-name\":\"Kensington\",\"id\":\"a37eca4d-c1e2-48a3-bcba-61796db97ebf\",\"name\":\"Kensington\",\"disambiguation\":\"\"},\"gender\":\"Male\",\"begin_area\":{\"sort-name\":\"Stone Town\",\"name\":\"Stone Town\",\"id\":\"71b1aedf-a1ad-4eb6-83a0-57134c8b34e2\",\"disambiguation\":\"\"},\"country\":\"GB\",\"area\":{\"disambiguation\":\"\",\"name\":\"United Kingdom\",\"id\":\"8a754a16-0027-3a29-b6d7-2b40ea0481ed\",\"iso-3166-1-codes\":[\"GB\"],\"sort-name\":\"United Kingdom\"},\"gender-id\":\"36d3d30a-839d-3eda-8cb3-29be4384e4a9\",\"isnis\":[\"0000000109859100\"],\"disambiguation\":\"\"}";
        final String camel = "{\"id\":\"022589ac-7177-460d-a178-9976cf70e29f\",\"name\":\"Freddie Mercury\",\"type\":\"Person\",\"gender\":\"Male\",\"country\":\"GB\",\"area\":{\"id\":\"8a754a16-0027-3a29-b6d7-2b40ea0481ed\",\"name\":\"United Kingdom\",\"disambiguation\":\"\",\"sortName\":\"United Kingdom\",\"iso31661Codes\":[\"GB\"]},\"disambiguation\":\"\",\"ipis\":[\"00077406269\"],\"isnis\":[\"0000000109859100\"],\"sortName\":\"Mercury, Freddie\",\"typeId\":\"b6e035f4-3ce9-331c-97df-83397230b0df\",\"genderId\":\"36d3d30a-839d-3eda-8cb3-29be4384e4a9\",\"lifeSpan\":{\"begin\":\"1946-09-05\",\"end\":\"1991-11-24\",\"ended\":true},\"beginArea\":{\"id\":\"71b1aedf-a1ad-4eb6-83a0-57134c8b34e2\",\"name\":\"Stone Town\",\"disambiguation\":\"\",\"sortName\":\"Stone Town\"},\"endArea\":{\"id\":\"a37eca4d-c1e2-48a3-bcba-61796db97ebf\",\"name\":\"Kensington\",\"disambiguation\":\"\",\"sortName\":\"Kensington\"}}";
        String result = roundtrip(kebabAndSpaghetti, MusicBrainzResponse.class);
        assertThat(result, is(camel));
    }
    @Test
    public void deserializeShouldIgnoreUnknownPropertiesMusicBrainz() throws Throwable {
        assertIgnoresUnknownProperties(MUSIC_BRAINZ, node -> Json.deserialize(Json.serialize(node), MusicBrainzResponse.class));
    }

    // URL
    @Test
    public void testRoundTripURLOtO() throws Exception {
        Url before = URL;
        Url after = roundtrip(before, Url.class);
        assertThat(after, is(before));
    }
    @Test
    public void shouldConverKebabToCamelURL() throws Exception {
        final String kebabAndSpaghetti = "{\"id\":\"8af26be7-92ff-4cbd-94be-70aba93c2b8c\",\"resource\":\"https://www.bbc.co.uk/music/artists/022589ac-7177-460d-a178-9976cf70e29f\"}";
        final String camel = "{\"id\":\"8af26be7-92ff-4cbd-94be-70aba93c2b8c\",\"resource\":\"https://www.bbc.co.uk/music/artists/022589ac-7177-460d-a178-9976cf70e29f\"}";
        String result = roundtrip(kebabAndSpaghetti, Url.class);
        assertThat(result, is(camel));
    }
    @Test
    public void deserializeShouldIgnoreUnknownPropertiesURL() throws Throwable {
        assertIgnoresUnknownProperties(URL, node -> Json.deserialize(Json.serialize(node), Url.class));
    }

    // RELATION
    @Test
    public void testRoundTripRelationOtO() throws Exception {
        Relation before = RELATION;
        Relation after = roundtrip(before, Relation.class);
        assertThat(after, is(before));
    }
    @Test
    public void shouldConvertKebabToCamelRelation() throws Exception {
        final String kebabAndSpaghetti = "{\"url\":{\"id\":\"8af26be7-92ff-4cbd-94be-70aba93c2b8c\",\"resource\":\"https://www.bbc.co.uk/music/artists/022589ac-7177-460d-a178-9976cf70e29f\"},\"target-type\":\"url\",\"target-credit\":\"\",\"attributes\":[],\"begin\":null,\"type\":\"BBC Music page\",\"direction\":\"forward\",\"attribute-values\":{},\"end\":null,\"type-id\":\"d028a975-000c-4525-9333-d3c8425e4b54\",\"source-credit\":\"\",\"ended\":false,\"attribute-ids\":{}}";
        final String camel = "{\"type\":\"BBC Music page\",\"url\":{\"id\":\"8af26be7-92ff-4cbd-94be-70aba93c2b8c\",\"resource\":\"https://www.bbc.co.uk/music/artists/022589ac-7177-460d-a178-9976cf70e29f\"},\"direction\":\"forward\",\"ended\":false,\"typeId\":\"d028a975-000c-4525-9333-d3c8425e4b54\",\"sourceCredit\":\"\",\"targetType\":\"url\",\"targetCredit\":\"\"}";
        String result = roundtrip(kebabAndSpaghetti, Relation.class);
        assertThat(result, is(camel));
    }
    @Test
    public void deserializeShouldIgnoreUnknownPropertiesRelation() throws Throwable {
        assertIgnoresUnknownProperties(RELATION, node -> Json.deserialize(Json.serialize(node), Relation.class));
    }

    // RELEASE GROUP
    @Test
    public void testRoundTripReleaseGroupOtO() throws Exception {
        ReleaseGroup before = RELEASE_GROUP;
        ReleaseGroup after = roundtrip(before, ReleaseGroup.class);
        assertThat(after, is(before));
    }
    @Test
    public void shouldConvertKebabToCamelReleaseGroup() throws Exception {
        final String kebabAndSpaghetti = "{\"id\":\"0c6da806-bdfe-3e64-88ed-74e778b6bfac\",\"primary-type\":\"Album\",\"primary-type-id\":\"f529b476-6e62-324f-b0aa-1f3e33d313fc\",\"first-release-date\":\"1998\",\"disambiguation\":\"\",\"title\":\"Romantic Ballads\",\"secondary-type-ids\":[\"dd2a21e1-0c00-3729-a7a0-de60b84eb5d1\"],\"secondary-types\":[\"Compilation\"]}";
        final String camel = "{\"id\":\"0c6da806-bdfe-3e64-88ed-74e778b6bfac\",\"title\":\"Romantic Ballads\",\"disambiguation\":\"\",\"primaryTypeId\":\"f529b476-6e62-324f-b0aa-1f3e33d313fc\",\"primaryType\":\"Album\",\"firstReleaseDate\":\"1998\",\"secondaryTypeIds\":[\"dd2a21e1-0c00-3729-a7a0-de60b84eb5d1\"],\"secondaryTypes\":[\"Compilation\"]}";
        String result = roundtrip(kebabAndSpaghetti, ReleaseGroup.class);
        assertThat(result, is(camel));
    }
    @Test
    public void deserializeShouldIgnoreUnknownPropertiesReleaseGroup() throws Throwable {
        assertIgnoresUnknownProperties(RELEASE_GROUP, node -> Json.deserialize(Json.serialize(node), ReleaseGroup.class));
    }

    // MUSIC BRAINZ FULL
    @Test
    public void testRoundTripMusicBrainzFullOtO() throws Exception {
        MusicBrainzResponse before = MUSIC_BRAINZ_FULL;
        MusicBrainzResponse after = roundtrip(before, MusicBrainzResponse.class);
        assertThat(after, is(before));
    }

    @Test
    public void deserializeShouldIgnoreUnknownPropertiesMusicBrainzFull() throws Throwable {
        assertIgnoresUnknownProperties(MUSIC_BRAINZ_FULL, node -> Json.deserialize(Json.serialize(node), MusicBrainzResponse.class));
    }

}