package com.viaplay.artistservice.client.musicbrainz;


import com.github.tomakehurst.wiremock.junit.WireMockClassRule;
import com.viaplay.artistservice.model.musicbrainz.MusicBrainzResponse;
import com.viaplay.artistservice.util.Json;
import lombok.extern.slf4j.Slf4j;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.TestPropertySourceUtils;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static com.viaplay.artistservice.util.TestData.MUSIC_BRAINZ;
import static com.viaplay.artistservice.util.TestData.MUSIC_BRAINZ_FULL;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@Slf4j
@SpringBootTest(properties = {"feign.hystrix.enabled=true"})
@ContextConfiguration(initializers = MusicBrainzClientTest.RandomPortInitializer.class)
public class MusicBrainzClientTest {
    @Autowired
    public MusicBrainzClient musicBrainzClient;

    @ClassRule
    public static WireMockClassRule wiremock = new WireMockClassRule(wireMockConfig().dynamicPort());

    @Test
    public void testMusicBrainzById() throws Exception {
        String json = Json.serialize(MUSIC_BRAINZ_FULL);
        final String id = MUSIC_BRAINZ_FULL.getId();
        stubFor(get(urlEqualTo(String.format("/%s?inc=url-rels+release-groups&type=album", id)))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON.toString())
                        .withBody(json)));
        MusicBrainzResponse result = musicBrainzClient.musicBrainzById(id).getBody();

        assertNotNull("should not be null", result);
        assertThat(result, is(MUSIC_BRAINZ_FULL));
    }

    @Test
    public void testMusicBrainzByIdFallback() {
        final String id = MUSIC_BRAINZ.getId();
        stubFor(get(urlEqualTo(String.format("/%s?inc=url-rels+release-groups&type=album", id)))
                .willReturn(aResponse().withFixedDelay(60000)));
        MusicBrainzResponse result = musicBrainzClient.musicBrainzById(id).getBody();

        assertNotNull("should not be null", result);
        assertThat(result.getId(), is("HISTRYX FALLBACK"));
    }

    // todo: tests for different client responses (4XX, 5XX ...)

    public static class RandomPortInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext applicationContext) {
            TestPropertySourceUtils
                    .addInlinedPropertiesToEnvironment(
                            applicationContext,
                            "musicbrainz.url=" + "http://localhost:" + wiremock.port()
                    );
        }
    }
}