package com.viaplay.artistservice.client.discogs;

import com.github.tomakehurst.wiremock.junit.WireMockClassRule;
import com.viaplay.artistservice.model.discogs.DiscogsResponse;
import com.viaplay.artistservice.util.Json;
import lombok.extern.slf4j.Slf4j;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.TestPropertySourceUtils;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static com.viaplay.artistservice.util.TestData.DISCOGS_RESPONSE;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@Slf4j
@SpringBootTest(properties = {"feign.hystrix.enabled=true"})
@ContextConfiguration(initializers = DiscogsClientTest.RandomPortInitializer.class)
public class DiscogsClientTest {
    @Autowired
    DiscogsClient discogsClient;
    @ClassRule
    public static WireMockClassRule wiremock = new WireMockClassRule(wireMockConfig().dynamicPort());

    @Test
    public void artistInfobyId() throws Exception {
        final String json = Json.serialize(DISCOGS_RESPONSE);
        final String id = "79949";
        stubFor(get(urlEqualTo(String.format("/%s", id)))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON.toString())
                        .withBody(json)));
        DiscogsResponse result = discogsClient.artistInfobyId(id).getBody();
        assertNotNull("should not be null", result);
        assertThat(result, is(DISCOGS_RESPONSE));
    }

    @Test
    public void testCoverArtByIdFallback() {
        final String id = "79949";
        stubFor(get(urlEqualTo(String.format("/%s", id)))
                .willReturn(aResponse().withFixedDelay(60000)));
        DiscogsResponse result = discogsClient.artistInfobyId(id).getBody();

        assertNotNull("should not be null", result);
        assertThat(result.getId(), is("HYSTRIX FALLBACK"));
    }

    public static class RandomPortInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext applicationContext) {
            TestPropertySourceUtils
                    .addInlinedPropertiesToEnvironment(
                            applicationContext,
                            "discogs.url=" + "http://localhost:" + wiremock.port()
                    );
        }
    }
}