package com.viaplay.artistservice.client.coverart;

import com.github.tomakehurst.wiremock.junit.WireMockClassRule;
import com.viaplay.artistservice.model.coverart.CoverArtResponse;
import com.viaplay.artistservice.util.Json;
import lombok.extern.slf4j.Slf4j;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.TestPropertySourceUtils;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static com.viaplay.artistservice.util.TestData.COVER_ART_RESPONSE;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@Slf4j
@SpringBootTest(properties = {"feign.hystrix.enabled=true"})
@ContextConfiguration(initializers = CoverArtClientTest.RandomPortInitializer.class)
public class CoverArtClientTest {
    @Autowired
    public CoverArtClient coverArtClient;
    @ClassRule
    public static WireMockClassRule wiremock = new WireMockClassRule(wireMockConfig().dynamicPort());

    @Test
    public void testCoverArtById() throws Exception {
        final String json = Json.serialize(COVER_ART_RESPONSE);
        final String id = "0c6da806-bdfe-3e64-88ed-74e778b6bfac";
        stubFor(get(urlEqualTo(String.format("/%s", id)))
                .willReturn(aResponse()
                        .withStatus(HttpStatus.OK.value())
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON.toString())
                        .withBody(json)));
        final CoverArtResponse result = coverArtClient.coverArtById(id).getBody();

        assertNotNull("should not be null", result);
        assertThat(result, is(COVER_ART_RESPONSE));
    }

    @Test
    public void testCoverArtByIdFallback() {
        final String id = "0c6da806-bdfe-3e64-88ed-74e778b6bfac";
        stubFor(get(urlEqualTo(String.format("/%s", id)))
                .willReturn(aResponse().withFixedDelay(60000)));
        final CoverArtResponse result = coverArtClient.coverArtById(id).getBody();

        assertNotNull("should not be null", result);
        assertThat(result.getRelease(), is("HYSTRIX FALLBACK"));
    }

    public static class RandomPortInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext applicationContext) {
            TestPropertySourceUtils
                    .addInlinedPropertiesToEnvironment(
                            applicationContext,
                            "coverart.url=" + "http://localhost:" + wiremock.port()
                    );
        }
    }
}