package com.viaplay.artistservice.client.discogs;

import com.viaplay.artistservice.config.FeignConfig;
import com.viaplay.artistservice.model.discogs.DiscogsResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@FeignClient(
        name = "DiscogsClient",
        url = "${discogs.url}",
        configuration = FeignConfig.class,
        fallbackFactory = DiscogsFallbackFactory.class
)
public interface DiscogsClient {
    @RequestMapping(value = "/{id}", method = GET, headers = "Accept=application/json")
    ResponseEntity<DiscogsResponse> artistInfobyId(@PathVariable("id") String id);
}
