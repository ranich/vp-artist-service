package com.viaplay.artistservice.client.discogs;

import com.viaplay.artistservice.exceptions.RestApiException;
import com.viaplay.artistservice.model.discogs.DiscogsResponse;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DiscogsFallbackFactory implements FallbackFactory<DiscogsClient> {
    @Override
    public DiscogsClient create(Throwable throwable) {
        return id -> {
            if (throwable instanceof RestApiException) {
                log.info("Got RestApi exception");
                return ResponseEntity.status(((RestApiException) throwable).getStatusCode()).build();
            }
        /*
        if request times out or cause not an instance of RestApiException handle it here
        todo: in case of timeout, return cached response if exists in cache.
        */
            log.info("Hystrix fallback");
            return ResponseEntity.ok(DiscogsResponse.builder().id("HYSTRIX FALLBACK").build());
        };
    }
}
