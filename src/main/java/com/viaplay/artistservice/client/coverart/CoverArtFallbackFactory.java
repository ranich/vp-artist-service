package com.viaplay.artistservice.client.coverart;

import com.viaplay.artistservice.exceptions.RestApiException;
import com.viaplay.artistservice.model.coverart.CoverArtResponse;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CoverArtFallbackFactory implements FallbackFactory<CoverArtClient> {
    @Override
    public CoverArtClient create(Throwable cause) {
        return id -> {
            if (cause instanceof RestApiException) {
                return ResponseEntity.status(((RestApiException) cause).getStatusCode()).build();
            }
            /*
            if request times out or cause not an instance of RestApiException handle it here
            todo: in case of timeout, return cached response if exists in cache.
            */
            log.info("Hystrix");
            return ResponseEntity.ok(CoverArtResponse.builder().release("HYSTRIX FALLBACK").build());
        };
    }
}
