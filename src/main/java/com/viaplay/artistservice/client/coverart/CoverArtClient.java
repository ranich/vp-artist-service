package com.viaplay.artistservice.client.coverart;

import com.viaplay.artistservice.config.FeignConfig;
import com.viaplay.artistservice.model.coverart.CoverArtResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@FeignClient(
        name = "CoverArtClient",
        url = "${coverart.url}",
        configuration = FeignConfig.class,
        fallbackFactory = CoverArtFallbackFactory.class
)
public interface CoverArtClient {
    @RequestMapping(value = "/{id}", method = GET, headers = "Accept=application/json")
    ResponseEntity<CoverArtResponse> coverArtById(@PathVariable("id") String id);
}
