package com.viaplay.artistservice.client.musicbrainz;

import com.viaplay.artistservice.config.FeignConfig;
import com.viaplay.artistservice.model.musicbrainz.MusicBrainzResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@FeignClient(
        name = "MusicClient",
        url = "${musicbrainz.url}",
        fallbackFactory = MusicBrainzFallbackFactory.class,
        configuration = FeignConfig.class
)
public interface MusicBrainzClient {
    @RequestMapping(value = "/{id}?inc=url-rels+release-groups&type=album", method = GET, headers = "Accept=application/json")
    ResponseEntity<MusicBrainzResponse> musicBrainzById(@PathVariable("id") String id);
}
