package com.viaplay.artistservice.model;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class MusicAlbum implements Serializable {
    private String id;
    private String title;
    private String image;
}
