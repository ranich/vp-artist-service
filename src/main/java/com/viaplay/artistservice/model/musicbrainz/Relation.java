package com.viaplay.artistservice.model.musicbrainz;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Relation {
    private String typeId;
    private String type;
    private Url url;
    private String sourceCredit;
    private String targetType;
    private String targetCredit;
    private String direction;
    private boolean ended;

    @JsonCreator
    public Relation(
            @JsonProperty("type-id") String typeId,
            @JsonProperty("type") String type,
            @JsonProperty("url") Url url,
            @JsonProperty("source-credit") String sourceCredit,
            @JsonProperty("target-type") String targetType,
            @JsonProperty("target-credit") String targetCredit,
            @JsonProperty("direction") String direction,
            @JsonProperty("ended") boolean ended) {
        this.typeId = typeId;
        this.type = type;
        this.url = url;
        this.sourceCredit = sourceCredit;
        this.targetType = targetType;
        this.targetCredit = targetCredit;
        this.direction = direction;
        this.ended = ended;
    }
}
