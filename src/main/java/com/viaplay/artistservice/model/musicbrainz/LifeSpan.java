package com.viaplay.artistservice.model.musicbrainz;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LifeSpan {
    private String begin;
    private String end;
    private boolean ended;

    @JsonCreator
    public LifeSpan(
            @JsonProperty("begin") String begin,
            @JsonProperty("end") String end,
            @JsonProperty("ended") boolean ended
    ) {
        this.begin = begin;
        this.end = end;
        this.ended = ended;
    }
}
