package com.viaplay.artistservice.model.musicbrainz;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Url {
    private String id;
    private String resource;

    @JsonCreator
    public Url(
            @JsonProperty("id") String id,
            @JsonProperty("resource") String resource) {
        this.id = id;
        this.resource = resource;
    }
}
