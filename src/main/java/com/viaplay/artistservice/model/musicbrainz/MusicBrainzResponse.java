package com.viaplay.artistservice.model.musicbrainz;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder(toBuilder = true)
public class MusicBrainzResponse {
    private String id;
    private String name;
    private String sortName;
    private String typeId;
    private String type;
    private String genderId;
    private String gender;
    private LifeSpan lifeSpan;
    private String country;
    private Area area;
    private BeginArea beginArea;
    private Area endArea;
    private String disambiguation;
    private List<String> ipis;
    private List<String> isnis;
    private List<Relation> relations;
    private List<ReleaseGroup> releaseGroups;

    @JsonCreator
    public MusicBrainzResponse(
            @JsonProperty("id") String id,
            @JsonProperty("name") String name,
            @JsonProperty("sort-name") String sortName,
            @JsonProperty("type-id") String typeId,
            @JsonProperty("type") String type,
            @JsonProperty("gender-id") String genderId,
            @JsonProperty("gender") String gender,
            @JsonProperty("life-span") LifeSpan lifeSpan,
            @JsonProperty("country") String country,
            @JsonProperty("area") Area area,
            @JsonProperty("begin_area") BeginArea beginArea,
            @JsonProperty("end_area") Area endArea,
            @JsonProperty("disambiguation") String disambiguation,
            @JsonProperty("ipis") List<String> ipis,
            @JsonProperty("isnis") List<String> isnis,
            @JsonProperty("relations") List<Relation> relations,
            @JsonProperty("release-groups") List<ReleaseGroup> releaseGroups) {
        this.id = id;
        this.name = name;
        this.sortName = sortName;
        this.typeId = typeId;
        this.type = type;
        this.gender = gender;
        this.genderId = genderId;
        this.lifeSpan = lifeSpan;
        this.country = country;
        this.area = area;
        this.beginArea = beginArea;
        this.endArea = endArea;
        this.disambiguation = disambiguation;
        this.ipis = ipis;
        this.isnis = isnis;
        this.relations = relations;
        this.releaseGroups = releaseGroups;
    }
}
