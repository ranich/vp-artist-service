package com.viaplay.artistservice.model.musicbrainz;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class BeginArea {
    private String id;
    private String sortName;
    private String name;
    private String disambiguation;
    private List<String> iso31662Codes;

    @JsonCreator
    public BeginArea(
            @JsonProperty("id") String id,
            @JsonProperty("sort-name") String sortName,
            @JsonProperty("name") String name,
            @JsonProperty("disambiguation") String disambiguation,
            @JsonProperty("iso-3166-2-codes") List<String> iso31662Codes) {
        this.id = id;
        this.sortName = sortName;
        this.name = name;
        this.disambiguation = disambiguation;
        this.iso31662Codes = iso31662Codes;
    }
}
