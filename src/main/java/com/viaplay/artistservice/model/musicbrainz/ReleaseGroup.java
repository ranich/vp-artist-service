package com.viaplay.artistservice.model.musicbrainz;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ReleaseGroup {

    private String id;
    private String title;
    private String primaryTypeId;
    private String primaryType;
    private String firstReleaseDate;
    private List<String> secondaryTypeIds;
    private List<String> secondaryTypes;
    private String disambiguation;

    @JsonCreator
    public ReleaseGroup(
            @JsonProperty("id") String id,
            @JsonProperty("title") String title,
            @JsonProperty("primary-type-id") String primaryTypeId,
            @JsonProperty("primary-type") String primaryType,
            @JsonProperty("first-release-date") String firstReleaseDate,
            @JsonProperty("secondary-type-ids") List<String> secondaryTypeIds,
            @JsonProperty("secondary-types") List<String> secondaryTypes,
            @JsonProperty("disambiguation") String disambiguation) {
        this.id = id;
        this.title = title;
        this.primaryTypeId = primaryTypeId;
        this.primaryType = primaryType;
        this.firstReleaseDate = firstReleaseDate;
        this.secondaryTypeIds = secondaryTypeIds;
        this.secondaryTypes = secondaryTypes;
        this.disambiguation = disambiguation;
    }
}
