package com.viaplay.artistservice.model.coverart;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class CoverArtResponse {
    private String release;
    private List<Image> images;
}
