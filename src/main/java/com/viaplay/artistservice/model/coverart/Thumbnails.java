package com.viaplay.artistservice.model.coverart;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Thumbnails {
    @JsonProperty("250")
    private String t250;
    @JsonProperty("500")
    private String t500;
    @JsonProperty("1200")
    private String t1200;
    private String small;
    private String large;
}
