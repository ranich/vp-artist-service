package com.viaplay.artistservice.model.coverart;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Image {
    private String id;
    private String image;
    private Thumbnails thumbnails;
    private long edit;
    private String comment;
    private List<String> types;
    private boolean front;
    private boolean approved;
    private boolean back;
}
