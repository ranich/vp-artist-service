package com.viaplay.artistservice.model.discogs;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DiscogsResponse {
    private String id;
    private String profile;
}
