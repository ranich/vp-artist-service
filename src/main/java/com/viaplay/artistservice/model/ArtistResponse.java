package com.viaplay.artistservice.model;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.List;

@Slf4j
@Data
@Builder
public class ArtistResponse implements Serializable {
    private String mbid;
    private String description;
    private List<MusicAlbum> albums;
}
