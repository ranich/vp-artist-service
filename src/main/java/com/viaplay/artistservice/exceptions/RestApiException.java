package com.viaplay.artistservice.exceptions;

import lombok.Getter;

@Getter
public class RestApiException extends Exception {
    private int statusCode;

    public RestApiException(int statusCode) {
        super();
        this.statusCode = statusCode;
    }

    public RestApiException(String message, int statusCode) {
        super(message);
        this.statusCode = statusCode;
    }
}
