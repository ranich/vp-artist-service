package com.viaplay.artistservice.config;

import com.viaplay.artistservice.exceptions.RestApiException;
import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

import static feign.FeignException.errorStatus;

@Slf4j
public class FeignErrorDecoder implements ErrorDecoder {

    // todo: Sedn all 2XX, 4XX client exceptions here

    @Override
    public Exception decode(String s, Response response) {
        final int statusCode = response.status();
        log.debug("Feign Error Decoder");
        Response.Body body = response.body();
        /*
        Try to read body to extract message
        try {
            if (body != null) {
                String s1 = Util.toString(body.asReader());
                log.error("body: {}", s1);
            }
        } catch (IOException e) {
            log.warn("couldnt decode string");
        }
        */
        if (statusCode == HttpStatus.NOT_FOUND.value()) {
            return new RestApiException(statusCode);
        }
        if (statusCode == HttpStatus.BAD_REQUEST.value()) {
            return new RestApiException(statusCode);
        }
        if (response.status() >= 500 && response.status() <= 599) {
            return new RestApiException(statusCode);
        }
        return errorStatus(s, response);
    }
}
