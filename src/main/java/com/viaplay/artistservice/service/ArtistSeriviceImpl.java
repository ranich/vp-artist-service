package com.viaplay.artistservice.service;

import com.viaplay.artistservice.client.coverart.CoverArtClient;
import com.viaplay.artistservice.client.discogs.DiscogsClient;
import com.viaplay.artistservice.client.musicbrainz.MusicBrainzClient;
import com.viaplay.artistservice.exceptions.RestApiException;
import com.viaplay.artistservice.model.ArtistResponse;
import com.viaplay.artistservice.model.MusicAlbum;
import com.viaplay.artistservice.model.coverart.CoverArtResponse;
import com.viaplay.artistservice.model.discogs.DiscogsResponse;
import com.viaplay.artistservice.model.musicbrainz.MusicBrainzResponse;
import com.viaplay.artistservice.model.musicbrainz.Relation;
import com.viaplay.artistservice.model.musicbrainz.ReleaseGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ArtistSeriviceImpl implements ArtistService {

    @Autowired
    MusicBrainzClient musicBrainzClient;

    @Autowired
    CoverArtClient coverArtClient;

    @Autowired
    DiscogsClient discogsClient;

    private static String discogsIdFromUrl(String url) {
        return url.replaceFirst(".*/([^/?]+).*", "$1");
    }

    @Override
    @Cacheable(value = "artist", key = "#mbid")
    public ArtistResponse artistResponse(String mbid) throws RestApiException {
        final ResponseEntity<MusicBrainzResponse> musicBrainzResponseResponseEntity = musicBrainzClient.musicBrainzById(mbid);
        final HttpStatus statusCode = musicBrainzResponseResponseEntity.getStatusCode();

        // If external API problem propagate Status Code.
        // todo: propagate exception messages from external api
        if (!statusCode.equals(HttpStatus.OK)) {
            throw new RestApiException(statusCode.value());
        }
        final MusicBrainzResponse body = musicBrainzResponseResponseEntity.getBody();
        if (body == null || body.getId() == null || !body.getId().equals(mbid)) {
            throw new RestApiException("Something went wrong", statusCode.value());
        }
        // #paranoid
        if (musicBrainzResponseResponseEntity.getBody().getId() == null || !mbid.equals(musicBrainzResponseResponseEntity.getBody().getId())) {
            throw new RestApiException("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR.value());
        }

        // ARTIST RESPONSE, setId
        final ArtistResponse.ArtistResponseBuilder artistBuilder = ArtistResponse.builder().mbid(mbid);

        final MusicBrainzResponse musicBrainzResponse = musicBrainzResponseResponseEntity.getBody();

        // GET ALBUM DATA
        final List<MusicAlbum> albumsResult = new ArrayList<>();
        final List<ReleaseGroup> albums = musicBrainzResponse.getReleaseGroups();
        // get albums ids from musicbrainz response, filter out empty values
        if (albums != null) {
            final List<String> albumsIds = albums
                    //.stream()
                    .parallelStream()
                    .map(ReleaseGroup::getId)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());

            final Map<String, ResponseEntity<CoverArtResponse>> collect = albumsIds
                    //.stream()
                    .parallelStream()
                    .collect(Collectors.toMap(id -> id, id -> coverArtClient.coverArtById(id)));

            // get a map of <id, AlbumImage>
            final Map<String, String> mapAlbumIdImageUrl = collect.entrySet()
                    //.stream()
                    .parallelStream()
                    // filter only OK coverArt response
                    .filter(map -> map.getValue().getStatusCode().equals(HttpStatus.OK))
                    // filter only non null image placeholder
                    .filter(map -> map.getValue().getBody() != null)
                    .filter(map -> map.getValue().getBody().getImages() != null)
                    .filter(map -> map.getValue().getBody().getImages().get(0).getImage() != null)
                    .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().getBody().getImages().get(0).getImage()));
            albums
                    .parallelStream()
                    //.stream()
                    .filter(album -> album.getId() != null)
                    .forEach(album -> albumsResult.add(
                            MusicAlbum.builder()
                                    .id(album.getId())
                                    .title(album.getTitle())
                                    .image(mapAlbumIdImageUrl
                                            .get(album.getId()))
                                    .build())
                    );
            // ARTIST RESPONSE, setAlbums
            artistBuilder.albums(albumsResult);
        }


        // GET ARTIST DESCRIPTION (FROM DISCOGS)

        // first, extract discogs artist id
        final List<Relation> relations = musicBrainzResponse.getRelations();
        if (musicBrainzResponseResponseEntity.getStatusCode().equals(HttpStatus.OK)) {
            final Optional<String> discogsArtistId = relations
                    //.stream()
                    .parallelStream()
                    .filter(relation -> relation.getType() != null)
                    .filter(relation -> relation.getType().equals("discogs"))
                    .filter(relation -> relation.getUrl() != null)
                    .filter(relation -> relation.getUrl().getResource() != null)
                    .map(relation -> discogsIdFromUrl(relation.getUrl().getResource()))
                    .findFirst();
            discogsArtistId.ifPresent(id -> {
                final ResponseEntity<DiscogsResponse> discogsResponseResponseEntity = discogsClient.artistInfobyId(id);
                if (discogsResponseResponseEntity.getStatusCode() == HttpStatus.OK
                        && discogsResponseResponseEntity.getBody() != null
                        && discogsResponseResponseEntity.getBody().getProfile() != null
                ) {
                    // ARTIST RESPONSE, setDescription
                    artistBuilder.description(discogsResponseResponseEntity.getBody().getProfile());
                }

            });
        }

        final ArtistResponse artistResponse = artistBuilder.build();
        return artistResponse;
    }
}
