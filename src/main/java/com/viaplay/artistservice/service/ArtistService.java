package com.viaplay.artistservice.service;

import com.viaplay.artistservice.exceptions.RestApiException;
import com.viaplay.artistservice.model.ArtistResponse;

public interface ArtistService {
    ArtistResponse artistResponse(String mbid) throws RestApiException;
}
