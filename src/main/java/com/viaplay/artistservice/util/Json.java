package com.viaplay.artistservice.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;

public class Json {
    public static ObjectMapper OBJECT_MAPPER = new ObjectMapper()
            .disable(FAIL_ON_UNKNOWN_PROPERTIES)
            .setSerializationInclusion(JsonInclude.Include.NON_NULL);

    public static <M> String serialize(M model) throws IOException {
        return OBJECT_MAPPER.writeValueAsString(model);
    }

    public static <M> M deserialize(String bytes, Class<? extends M> clazz) throws IOException {
        return OBJECT_MAPPER.readValue(bytes, clazz);
    }
}
