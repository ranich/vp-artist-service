package com.viaplay.artistservice.controller;

import com.viaplay.artistservice.exceptions.RestApiException;
import com.viaplay.artistservice.model.ArtistResponse;
import com.viaplay.artistservice.service.ArtistService;
import com.viaplay.artistservice.util.Json;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

import static java.util.Collections.singletonMap;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@Slf4j
public class ArtistController {
    @Autowired
    ArtistService artistService;

    @RequestMapping(
            value = "/api/v1/artists/{id:^[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{4}-[0-9A-Fa-f]{12}$}",
            method = GET,
            headers = "Accept=application/json")
    ResponseEntity<?> artist(@PathVariable("id") String id) {
        log.info("vo artists controller");
        try {
            final ArtistResponse artistResponse = artistService.artistResponse(id);
            return ResponseEntity.ok(artistResponse);
        } catch (RestApiException e) {
            log.info("RestApi Exception caught");
            final HttpStatus status = HttpStatus.valueOf(e.getStatusCode());

            if (status.is5xxServerError()) {
                return new ResponseEntity<>(
                        singletonMap("error", "Service is unavailable."),
                        HttpStatus.SERVICE_UNAVAILABLE);

            }
            if (status.equals(HttpStatus.NOT_FOUND)) {
                return new ResponseEntity<>(
                        singletonMap("error", String.format("Resource for id %s was not found on the server.", id)),
                        HttpStatus.NOT_FOUND);

            }
            if (status.equals(HttpStatus.BAD_REQUEST)) {
                try {
                    return new ResponseEntity<>(Json.serialize(e), HttpStatus.BAD_REQUEST);
                } catch (IOException e1) {
                    return new ResponseEntity<>(singletonMap("error", "Bad request"), HttpStatus.BAD_REQUEST);
                }
            }
            return ResponseEntity.status(e.getStatusCode()).build();
        }
    }
}
