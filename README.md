# Viaplay Demo - Artist Service


[Build and Run](#build-and-run)
- [Local](#local)
- [Docker](#docker)

[No build, just try it](#no-build-just-try-it)

[Example Ids](#example-ids)

[Technology](#technology)
  

## Build And Run

### Local


Clone the repo and cd to the service directory:

```bash
> git clone https://gitlab.com/ranich/vp-artist-service.git ; cd vp-artist-service
```

Run the service:


```bash
> ./mvnw spring-boot:run
```


Now the service is running in the default Jetty server locally available on a port 8080.

To check that everything is alright, visit `http://localhost:8080/actuator/health`. 

The service endpoint is available on `http://localhost:8080/api/v1/artists/{mbid}`.

To try it out, you can use Swagger, which is available on `http://localhost:8080/swagger-ui.html`

Or, from the terminal:

```
MBID="022589ac-7177-460d-a178-9976cf70e29f"
curl http://localhost:8080/api/v1/artists/$MBID | jq
```

## Docker

To dockerize the app, first we need to package it into a fat jar:

```bash
> mvn clean package
```

Now build the docker image. Make sure you are in the root of the directory

```bash
> docker build -t vp:artists .
```

Run it:

```bash
> docker run -d -it -p 9777:8080 vp:artists
```

This will start the service on `http://localhost:9777/api/v1/artists/{mbid}`

## No build just try it

The service is deployed in Amazon AWS. It's a simple setup of a single service instance, using the following components:

* **EC2**
* **Security Groups**
* **Redis**
* **Docker Compose**

Available Links:

Swagger: `http://ec2-18-130-125-233.eu-west-2.compute.amazonaws.com:8777/swagger-ui.html`

API Endpoint: `http://ec2-18-130-125-233.eu-west-2.compute.amazonaws.com:8777/api/v1/artists/{mbid}`


## Example Ids



```
022589ac-7177-460d-a178-9976cf70e29f
fd9e2877-c8a2-4572-80c1-144fe5f39ba0
b1220290-6ad5-40b5-8d78-c8d23ab240ae
022589ac-7177-460d-a178-9976cf70e29f
fd9e2877-c8a2-4572-80c1-144fe5f39ba0
b1220290-6ad5-40b5-8d78-c8d23ab240ae
```

Id generating not found:
```$xslt
b1220290-6ad5-40b5-8d78-c8d23ab240aa
```


## Technology

*  **Feign**  -  Declarative Rest Client. It fits elegantly with both external APIs, but also for own services. 
*  **Hystrix** -  Circuit breaker for external services
*  **Redis** - Caching. Currently deployed on EC2, this is the source of information for both
the local setup and the one running on EC2.
*  **Lombok**  -  Reduces boilerplate code in Java classes.
*  **Jackson** - For object serialization / deserialization
*  **Junit** - Unit testing framework
*  **WireMock** - a simulator for HTTP based APIs. Used for testing the external APIs
*  **Mockito** -  Mock and Stubs framework
*  **Swagger** - API Documentation
* **Spring Actuator** - Exposes operational information about our app - health, metrics, info etc..

## Application Current State 

- Regarding final payload being served. I didn't have the time to make a through exploration on profile information
 sources to use, and went with the suggested one, discogs. If I had more time, I would have probably make some combination 
considering another source or more that would fill in the required profile descrption. Of course business needs are 
important, if having missing data is acceptable, or we extend the request calls for getting optimal result.

- I have omitted some test coverages in the Client Tests (External API Clients)

- There are things to improve regarding exception handling.

- If I were about to put this in production, fitting it in a microservice world, there are quite some things I would do.
Maybe discussion fits better for this one.